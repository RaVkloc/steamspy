import datetime
import sys
import json
import requests

player_status = {0: "Offline or Private profile",
                 1: "Online",
                 2: "Busy",
                 3: "Away",
                 4: "Snooze",
                 5: "Looking to trade",
                 6: "Looking to play"}


def calculate_time_difference(timestamp: int):
    delta = datetime.datetime.now() - datetime.datetime.fromtimestamp(timestamp)
    hours = int(delta.seconds // 3600)

    if delta.days >= 2:
        return f"{delta.days} days"
    if delta.days == 1:
        return f"1 day {int(delta.seconds // 3600)} hours"

    minutes = int(delta.seconds // 60) % 60

    if hours > 0:
        return f"{hours} hours {minutes} minutes"

    return f"{minutes} minutes"


def get_player_info(api_key: str, steam_id: list[str]):
    url = f"https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={api_key}&steamids={','.join(steam_id)}"
    response = requests.get(url)

    if response.status_code == 200:
        data = json.loads(response.text)["response"]["players"]

        for player in data:
            print(f"Details for player {player['personaname']}:")
            if player["communityvisibilitystate"] == 1:
                print("\tYou cannot see this profile - it is private or friends only.")
                print()
                continue

            print(f"\tStatus: {player_status[player['personastate']]}")
            print("\tLast online:", f"{datetime.datetime.fromtimestamp(player['lastlogoff'])}",
                  f"({calculate_time_difference(player['lastlogoff'])} ago)" if player['personastate'] == 0 else "")
            print(f"\tIn game: {player.get('gameextrainfo', '-')}")
            print()

    else:
        sys.stderr.write("Unable to fetch information. Passed API key is invalid probably.")
        exit(-1)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write("Missing arguments.\nUsage: main.py <API_KEY> <STEAM_ID> ...")

    max_ids_in_one_request = 100
    steam_ids = [sys.argv[i:i + max_ids_in_one_request] for i in
                 range(2, 2 + len(sys.argv[2:]), max_ids_in_one_request)]

    for ids in steam_ids:
        get_player_info(sys.argv[1], ids)
