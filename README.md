# SteamSpy
<!-- TOC -->
* [SteamSpy](#steamspy)
  * [How to run?](#how-to-run)
  * [How to get your Steam ID?](#how-to-get-your-steam-id)
  * [How to get your friend's Steam ID?](#how-to-get-your-friends-steam-id)
<!-- TOC -->


## How to run?
To run application you must have installed Python 3 on your system and follow steps:
1. Go to https://steamcommunity.com/dev/apikey and generate your own API key.
2. Run `pip install -r requirements.txt` to get all necessary dependencies.
3. Run `python3 main.py` passing API key as first argument and players' IDs - you can pass as many IDs as you want to.


## How to get your Steam ID?
All steps have to be taken in desktop application.
1. Click in top right corner on your avatar.
2. Click in "Account detail"
3. Steam ID will be listed on top of the page.

## How to get your friend's Steam ID?
All steps have to be taken in desktop application.

1. Enable showing URLs in app:
   - In left top corner click Steam -> Settings -> Interface
   - Tick the box: "Display Steam URL address bar when available"
2. Click on your nickname and select "Friends" from drop list.
3. Click on friend you want to spy
4. From displayed URL: `https://steamcommunity.com/profiles/XXXXXXXXXXXXXXXXX` copy part marked as Xs in example.